import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Animated
} from 'react-native';

export default class Splash extends Component {

  constructor(){
    super()
    this.state ={
      splashanim: new Animated.Value(0)
    }
  }

  componentWillMount () {
    Animated.sequence([
      Animated.timing(this.state.splashanim,  {
        toValue: 1,
        duration: 2400
      }),
    ]).start()
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          CodeHow teaches you...
        </Text>
        <Animated.View style={{ opacity: this.state.splashanim }}>
      <Image style={styles.logo}source={require('../img/reactlogo.png')} />
      <Text style={styles.welcome}> REACT NATIVE </Text>
      </Animated.View>
      <Text style={styles.description}> Un framework para crear apliaciones nativas con React</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  logo:{
    width: 250,
    height: 250,
    margin: 0
  },
  description:{
    fontSize: 10,
    textAlign: 'center',
    margin: 5,  }
});

AppRegistry.registerComponent('Splash', () => Splash);
