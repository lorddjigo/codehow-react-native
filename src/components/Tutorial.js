import React, { Component } from 'react';
import {
AppRegistry,
StyleSheet,
Text,
View,
Image
}from 'react-native';

export default class Tutorial extends Component{
  render(){
    return(
      <View style ={styles.container}>
      <Text style={ styles.h1 }>Bien, empecemos por lo básico </Text>

      <Text style={styles.p}>  React Native es un __ desarrollado que nos permite crear apliaciones utilizando únicamente JavaScript (Ese lenguaje
        que nos permite interactuar y programar nuestras páginas web).  </Text>
        <Text style={styles.p}> ¿Lo más importante? </Text>

        <Text style={styles.p}> Las aplicaciones que crearemos serán  NATIVAS </Text>
        <Text style={styles.p}> ¿Y qué significa? Que está optimizada para el sistema operativo,
         adaptandose al 100%.  Lo más óptimo si queremos rendimiento. </Text>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },

  h1:{
    textAlign: 'center',
    fontSize: 15
  },

  p:{
    fontSize:12,
  }

});

AppRegistry.registerComponent('Tutorial', () => Tutorial);
