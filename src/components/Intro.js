import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Button,
  TouchableHighlight
} from 'react-native';

export default class Intro extends Component {
  constructor(){
    super()
    this.state ={
      logoanim: new Animated.Value(400),
      helloanim: new Animated.Value(0),
      textanim: new Animated.Value(0),
      buttonanim: new Animated.Value(0)
    }
  }

  componentWillMount () {
    Animated.sequence([
      Animated.timing(this.state.logoanim,  {
        toValue: 200,
        duration: 2400
      }),

      Animated.timing( this.state.helloanim, {
        toValue: 1,
        duration: 3000
      }),

      Animated.timing( this.state.textanim, {
        toValue: 1,
        duration: 2000
      }),
      Animated.timing( this.state.buttonanim, {
        toValue: 1,
        duration: 3000
      }),
    ]).start()
  }

  render() {
    return (
      <View style={styles.container}>
      <Animated.Image style={{width: this.state.logoanim, height: this.state.logoanim, margin: 0}}source={require('../img/reactlogo.png')} />
        <Animated.View style={{ opacity: this.state.helloanim }}>
        <Text style={styles.h1}>
        Antes que nada... ¿sabes lo que es React Native?
        </Text>
          </Animated.View>
        <Animated.View style={{ opacity: this.state.textanim }}>
        <Text style={styles.h2}>Bueno, ¡estoy aquí para ayudarte!</Text>
        </Animated.View>

        <Animated.View style={{ opacity: this.state.buttonanim}}>
        <TouchableHighlight >
           <Text style={styles.button}>¡ Enséñame en qué consiste !</Text>
         </TouchableHighlight>

  <TouchableHighlight >
     <Text style={styles.skiptext} > O pulsa aquí si ya conoces lo básico.</Text>
   </TouchableHighlight>

   </Animated.View>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  h1: {
    fontSize: 20,
    textAlign: 'center',
    margin:10,
  },
  h2: {
    fontSize: 15,
    textAlign: 'center',
    marginBottom: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },

  button:{
    textAlign: 'center',
    fontSize: 17,
    backgroundColor: '#b4dde8',
    marginTop: 50,
    padding: 12,
    width: 250,
    color: '#fff'
  },

  skiptext:{
    opacity: 0.2,
    fontSize: 16,
    marginTop: 40,
    textDecorationLine: 'underline'
  },

});

AppRegistry.registerComponent('Intro', () => Intro);
