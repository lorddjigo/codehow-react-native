/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import Splash from './src/components/Splash';
import Intro from './src/components/Intro';

export default class CodeHow extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Intro/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  logo:{
    width: 250,
    height: 250,
  },
  description:{
    fontSize: 10,
    textAlign: 'center',
    margin: 5,  }
});

AppRegistry.registerComponent('CodeHow', () => CodeHow);
